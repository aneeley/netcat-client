#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "nc.h"

int nc_connect(char *address, int port, char *resp)
{
    int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (sockfd == -1) {
        fprintf(stderr, "Can't create socket\n");
        exit(3);
    }

    struct sockaddr_in sa;

    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    inet_pton(AF_INET, address, &sa.sin_addr);
    
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));

    if (res == -1) {
        fprintf(stderr, "Can't connect\n");
        exit(2);
    }

    char buf[16];
    nc_recv(sockfd, buf);
    strcpy(resp, buf+4);

    return sockfd;
}

void nc_close(int sockfd)
{
    close(sockfd);
}

void nc_recv(int sockfd, char *buf)
{
    ssize_t r_size = recv(sockfd, buf, BUF_SIZE, 0);
    buf[r_size] = 0;
}

void nc_send(int sockfd, char *msg, char *buf)
{
    ssize_t s_size = send(sockfd, msg, strlen(msg), 0);
    nc_recv(sockfd, buf);
}

void nc_list(int sockfd, char **resp, int *num)
{
    char buf[256];

    nc_send(sockfd, "LIST\n", buf);
    puts(buf);
    nc_recv(sockfd, buf);
    puts(buf);

    char *tok;
    int i = 0;

    for(tok = strtok(buf, "\n");
            tok[0] != '.'; 
            tok = strtok(NULL, "\n"))
        resp[i++] = strdup(tok);
    *num = i;
}

void nc_size(int sockfd, char *filename, int *resp)
{
    char cmd[32];
    snprintf(cmd, 32, "SIZE %s\n", filename);
    char buf[32];
    nc_send(sockfd, cmd, buf);
    sscanf(buf+4, "%d", resp);
}

void nc_dl(int sockfd, char *filename)
{
    char buf[BUF_SIZE];
    int size;
    ssize_t r_size;
    char cmd[32];
    FILE* fp = fopen(filename, "wb");

    nc_size(sockfd, filename, &size);

    snprintf(cmd, 32, "GET %s\n", filename);
    nc_send(sockfd, cmd, buf);

    do {
        r_size = recv(sockfd, buf, BUF_SIZE, 0);
        buf[r_size] = 0;
        fwrite(buf, r_size, 1, fp);
        size -= r_size;
    }
    while(size > 0);

    fclose(fp);
}
