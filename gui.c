#include <string.h>
#include <stdlib.h>

#include "gui.h"

void init_ncurses()
{
    initscr();
    cbreak();
    noecho();
    curs_set(0);
    keypad(stdscr, TRUE);
    start_color();

    init_pair(1, COLOR_GREEN, COLOR_BLACK);
}

void print_in_center(WINDOW *win, int y, int w, char *msg)
{
    int len = strlen(msg);
    mvwprintw(win, y, (w - len) / 2 ,msg);
}

char *humanize_bytes(int bytes)
{
    char *byte_sizes[] = { "B", "KB", "MB", "GB" };
    int i = 0;

    while(bytes >> (10 * i) >= 1024) i++;

    char str[16];
    snprintf(str, 16, "%4d %s", bytes >> (i * 10), byte_sizes[i]);
    return strdup(str);
}
