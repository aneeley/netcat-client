#ifndef NC_H
#define NC_H

#define BUF_SIZE 1024

int nc_connect(char *address, int port, char *resp);
void nc_close(int sockfd);
void nc_recv(int sockfd, char *buf);
void nc_send(int sockfd, char *msg, char *buf);
void nc_list(int sockfd, char **resp, int *size);
void nc_size(int sockfd, char *filename, int *buf);
void nc_dl(int sockfd, char *filename);

#endif
