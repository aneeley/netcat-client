#ifndef GUI_H
#define GUI_H

#include <menu.h>

void init_ncurses();
void print_in_center(WINDOW *win, int y, int w, char *msg);
char *humanize_bytes(int bytes);

#endif
