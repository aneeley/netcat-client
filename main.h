#ifndef MAIN_H
#define MAIN_H

#include <menu.h>

int create_sockfd_from_args(int argc, char **argv);
void usage(char *progname);
void dl(char *filename);
void refresh_users_files(MENU *users_files_menu, char **files, int num_files);

#endif
