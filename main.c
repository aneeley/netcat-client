#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "main.h"
#include "nc.h"
#include "gui.h"

int main(int argc, char **argv)
{
    /* connect to netcat server */
    char resp[32];
    int s = create_sockfd_from_args(argc, argv);

    /* start ncurses */
    init_ncurses();

    /* get files */
    char *files[32];
    int num_files;
    nc_list(s, files, &num_files);

    /* get users files */
    char *users_files[32];
    int num_users_files = 0;
    int u_i = 0;
    for(int i = 0; i < num_files; i++)
        if( access( files[i], F_OK ) != -1 )
            users_files[num_users_files++] = files[i];

    /* get file sizes */
    char *file_sizes[32];
    for(int i = 0; i < num_files; i++)
    {
        int size;
        nc_size(s, files[i], &size);
        file_sizes[i] = humanize_bytes(size);
    }

    /* create menu items */
    ITEM **items = calloc(num_files + 1, sizeof(ITEM *));
    for(int i = 0; i < num_files; i++)
    {
        items[i] = new_item(files[i], file_sizes[i]);
        set_item_userptr(items[i], nc_dl);
    }
    items[num_files] = NULL;

    /* create users menu items */
    ITEM **user_items = calloc(num_users_files + 1, sizeof(ITEM *));
    for(int i = 0; i < num_users_files; i++)
        user_items[i] = new_item(users_files[i], "");
    user_items[num_users_files] = NULL;

    /* create menu */
    MENU *files_menu = new_menu(items);

    /* create user menu */
    MENU *users_files_menu = new_menu(user_items);

    /* create menu window */
    WINDOW *files_win = newwin(LINES, COLS/2, 0, 0);
    wattron(files_win, COLOR_PAIR(1));
    box(files_win, ACS_VLINE, ACS_HLINE);
    print_in_center(files_win, 0, COLS/2, "files");
    wattroff(files_win, COLOR_PAIR(1));
    set_menu_win(files_menu, files_win);
    set_menu_sub(files_menu, derwin(files_win, LINES-2, COLS/2-1, 1, 1));

    set_menu_mark(files_menu, NULL);
    
    /* create user menu window */
    WINDOW *users_files_win = newwin(LINES, COLS/2, 0, COLS/2);
    wattron(users_files_win, COLOR_PAIR(1));
    box(users_files_win, ACS_VLINE, ACS_HLINE);
    print_in_center(users_files_win, 0, COLS/2, "user's files");
    wattroff(users_files_win, COLOR_PAIR(1));
    set_menu_win(users_files_menu, users_files_win);
    set_menu_sub(users_files_menu, derwin(users_files_win, LINES-2, COLS/2-5, 1, 1));

    set_menu_mark(users_files_menu, NULL);

    /* add menus */
    post_menu(files_menu);
    post_menu(users_files_menu);
    refresh();
    wrefresh(files_win);
    wrefresh(users_files_win);

    int c;
    while((c = getch()) != 'q')
    {
        switch(c)
        {
            case 'k':
            case KEY_UP:
                menu_driver(files_menu, REQ_UP_ITEM);
                break;

            case 'j':
            case KEY_DOWN:
                menu_driver(files_menu, REQ_DOWN_ITEM);
                break;

			case 10: /* enter */
            {
                ITEM *cur;
				void (*p)(int, char*);

				cur = current_item(files_menu);
				p = item_userptr(cur);
				p(s, (char *)item_name(cur));
				pos_menu_cursor(files_menu);

                refresh_users_files(users_files_menu, files, num_files);
				break;
			}
        }
        wrefresh(files_win);
        wrefresh(users_files_win);
    }

    /* free memory */
    unpost_menu(files_menu);
    unpost_menu(users_files_menu);
    for(int i = 0; i < num_users_files; i++)
        free_item(user_items[i]);
    for(int i = 0; i < num_files; i++)
        free_item(items[i]);
    free_menu(files_menu);
    free_menu(users_files_menu);
    endwin();

    return 0;
}

void refresh_users_files(MENU *users_menu, char **files, int num_files)
{
    char *users_files[32];
    int num_users_files = 0;
    int u_i = 0;
    for(int i = 0; i < num_files; i++)
        if( access( files[i], F_OK ) != -1 )
            users_files[num_users_files++] = files[i];

    ITEM **user_items = calloc(num_users_files + 1, sizeof(ITEM *));
    for(int i = 0; i < num_users_files; i++)
        user_items[i] = new_item(users_files[i], "");
    user_items[num_users_files] = NULL;
    unpost_menu(users_menu);
    set_menu_items(users_menu, user_items);
    post_menu(users_menu);
}

int create_sockfd_from_args(int argc, char **argv)
{
    int sockfd;
    int port;
    char greeting[32];
    switch(argc)
    {
        case 1: sockfd = nc_connect("65.19.178.177", 1234, greeting); break;
        case 3: sscanf(argv[2], "%d", &port);
                sockfd = nc_connect(argv[1], port, greeting); break;
        default: usage(argv[0]); break;
    }
    return sockfd;
}

void usage(char *progname)
{
    fprintf(stderr, "Usage: %s 127.0.0.1 1234\nOr: %s\n",
            progname, progname); exit(1);
    return;
}
