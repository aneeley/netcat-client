CC = gcc
FLAGS = -g
OBJECTS = gui.o nc.o

all: main

main: main.c $(OBJECTS)
	$(CC) $(FLAGS) main.c $(OBJECTS) -o main -lmenu -lpanel -lncurses

gui.o: gui.c gui.h
	$(CC) $(FLAGS) -c gui.c -o gui.o

nc.o: nc.c nc.h
	$(CC) $(FLAGS) -c nc.c -o nc.o

ncat:
	netcat -v 65.19.178.177 1234

clean:
	rm -rf main *.o *.dSYM/
